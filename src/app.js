const Hapi = require("@hapi/hapi");
const Joi = require("joi");
const Jwt = require("@hapi/jwt");

require("dotenv").config();

const init = async () => {
  const server = Hapi.server({
    port: process.env.PORT,
    host: "localhost",
  });

  await server.register(Jwt);

  server.auth.strategy("my_jwt_strategy", "jwt", {
    keys: process.env.JWT_SECRET,
    verify: {
      aud: "urn:audience:test",
      iss: "urn:issuer:test",
      sub: false,
      nbf: true,
      exp: true,
      maxAgeSec: 14400, // 4 hours
      timeSkewSec: 15,
    },
    validate: (artifacts, request, h) => {
      return {
        isValid: true,
        credentials: { user: artifacts.decoded.payload.user },
      };
    },
  });

  server.auth.default("my_jwt_strategy");

  await server.register({
    plugin: require("./plugins/dbPlugin"),
  });

  server.route({
    method: "POST",
    path: "/login",
    handler: (request, h) => {
      return "Hello World!";
    },
    options: {
      validate: {
        payload: Joi.object({
          username: Joi.string().required(),
          password: Joi.string().required(),
        }),
      },
    },
  });

  server.route({
    method: "POST",
    path: "/logout",
    handler: (request, h) => {
      return h.response({ message: "Logged out successfully" });
    },
  });

  server.route({
    method: "POST",
    path: "/deposit",
    handler: async (request, h) => {},
  });

  server.route({
    method: "POST",
    path: "/withdrawal",
    handler: (request, h) => {
      return "Hello World!";
    },
  });

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
