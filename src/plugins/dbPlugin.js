const { Client } = require("pg");

module.exports = {
  name: "dbPlugin",
  version: "1.0.0",
  register: async function (server, options) {
    // Create a route for example

    const client = new Client({
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      database: process.env.DB_DATABASE,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
    });

    await client.connect();

    const q = client.query;
  },
};
